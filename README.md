# Made using Laravel Breeze + React

### Anagram algorithm
The code generates an alphabetical key for each word and then later selects anagrams from the database using that.

### WordController.php
Contains API functions. Used DRY programming principles by creating smaller functions to reuse.

### WordTest.php
Unit tests for testing out the anagram system

### Word Eloquent model
This is the model for the wordlist elements. Contains the word itself and it’s key. Word is used as a primary key.

### API routes
**POST** /api/words – accepts an array converted to JSON and reads all the words into database

**GET** /api/words/{word} – finds anagrams for {word} from database

### React front-end interface
After login has two main pages for anagrams and wordlist upload.

Find anagrams – search input using debounce to check for anagrams using the API, returns them in the same view as a comma separated list

Insert wordlist – Textarea for adding words or file add that accepts only *.txt files and reads them into the textarea. Each new line is a new word.
