<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Controllers\API\WordController;
use App\Models\Word;

class WordTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic test.
     *
     * @return void
     */
    public function test_anagram_func_basic()
    {

        $word1 = 'tokyo';
        $word2 = 'kyoto';

        $key = app('App\Http\Controllers\API\WordController')->keygen($word1);
        
        Word::factory()->create([
            'word' => $word1,
            'key' => $key
        ]);


        $return = app('App\Http\Controllers\API\WordController')->findAnagrams($word2);

        $this->assertEquals(array($word1), $return);

    }

    /**
     * Capitals test.
     *
     * @return void
     */
    public function test_anagram_func_capitals()
    {
        //Word1 being capitalized breaks the thing. All database words should be lowercase
        $word1 = 'tokyo';
        $word2 = 'Kyoto';

        $key = app('App\Http\Controllers\API\WordController')->keygen($word1);
        
        Word::factory()->create([
            'word' => $word1,
            'key' => $key
        ]);


        $return = app('App\Http\Controllers\API\WordController')->findAnagrams($word2);

        $this->assertEquals(array($word1), $return);

    }

    /**
     * Umlauts test.
     *
     * @return void
     */
    public function test_anagram_func_umlaut()
    {
        
        $word1 = 'õnnelik';
        $word2 = 'nneikõl';

        $key = app('App\Http\Controllers\API\WordController')->keygen($word1);
        
        Word::factory()->create([
            'word' => $word1,
            'key' => $key
        ]);


        $return = app('App\Http\Controllers\API\WordController')->findAnagrams($word2);

        $this->assertEquals(array($word1), $return);

    }
    

}