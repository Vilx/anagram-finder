<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Word;
use Illuminate\Http\Request;

class WordController extends Controller
{
    /**
     * Finds anagrams.
     *
     * @return Array
     */
    public function findAnagrams($word)
    {
        $word = strtolower($word);

        $key = WordController::keygen($word);

        $anagrams = Word::where('key', $key)->where('word', '!=', $word)->pluck('word')->toArray();

        return $anagrams;
    }

    /**
     * Key generator.
     *
     * @return String
     */
    public function keygen($word)
    {
        $word = strtolower($word);
        
        $wparts = mb_str_split($word);
        sort($wparts);
        $key = implode('', $wparts);

        return $key;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $array = json_decode($request->input('data'));

        foreach ($array as $word) {

            $word = strtolower($word);
            $word = preg_replace('/\s+/', '', $word);
            
            $key = WordController::keygen($word);


            Word::updateOrCreate(
                ['word' => $word],
                ['key' => $key]
            );
        }

        return response()->json([
            "message" => "Word(s) added"
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  String  $word
     * @return \Illuminate\Http\Response
     */
    public function show($word)
    {
        $anagrams = WordController::findAnagrams($word);

        return response()->json([
            "anagrams" => json_encode($anagrams)
        ], 201);
    }
}
