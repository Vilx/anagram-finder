import React, { useState } from "react";
import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import InputLabel from "@/Components/InputLabel";
import { DebounceInput } from "react-debounce-input";
import { Head } from "@inertiajs/inertia-react";
import { Transition } from "@headlessui/react";
import Api from "../Gateways/gateway";

export default function Anagram(props) {
    const [anagrams, setAnagrams] = useState("");

    const searchAnagram = async (word) => {
        if (word != "") {
            try {
                const response = await Api.get("/words/" + word, {});

                var anagrams = JSON.parse(response.data.anagrams);

                setAnagrams(anagrams.join(", "));
            } catch (error) {
                console.log(error);
            }
        }
    };

    return (
        <AuthenticatedLayout
            auth={props.auth}
            errors={props.errors}
            header={
                <h2 className="font-semibold text-xl text-gray-800 leading-tight">
                    Find anagrams
                </h2>
            }
        >
            <Head title="Anagram" />

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="px-8 pt-6 pb-8 mb-4">
                            <InputLabel
                                className="pb-2"
                                for="search"
                                value="Insert the word you want to find anagrams for"
                            />

                            <DebounceInput
                                id="search"
                                className="border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 rounded-md shadow-sm mt-1 block w-full"
                                minLength={2}
                                debounceTimeout={350}
                                onChange={(event) =>
                                    searchAnagram(event.target.value)
                                }
                            />

                            <Transition
                                show={anagrams != ""}
                                enterFrom="opacity-0"
                                leaveTo="opacity-0"
                                className="transition ease-in-out"
                            >
                                <div className="pt-6 text-gray-900">
                                    {anagrams}
                                </div>
                            </Transition>
                        </div>
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
