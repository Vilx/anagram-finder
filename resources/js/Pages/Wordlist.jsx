import React, { useState } from "react";
import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import InputLabel from "@/Components/InputLabel";
import { Head } from "@inertiajs/inertia-react";
import { Transition } from "@headlessui/react";
import Api from "../Gateways/gateway";

export default function Wordlist(props) {

    const [message, setMessage] = useState("");

    const onFileChange = () => {

        let files = document.querySelector("#file-input").files;

        if (files.length === 0) return;

        const file = files[0];

        let reader = new FileReader();

        reader.onload = (e) => {
            const file = e.target.result;
            document.querySelector("textarea").value = file;
        };

        reader.onerror = (e) => alert(e.target.error.name);

        reader.readAsText(file);

    };

    const handleSubmit = async (e) => {

        e.preventDefault();

        var inputArray = document.querySelector("textarea").value.split("\n");

        const perChunk = 50000;

        const result = inputArray.reduce((resultArray, item, index) => { 
        const chunkIndex = Math.floor(index/perChunk);

        if(!resultArray[chunkIndex]) {
            resultArray[chunkIndex] = [] 
        }

        resultArray[chunkIndex].push(item);

        return resultArray;
        }, []);

        const ids = [12, 32, 657, 1, 67];
        const promises = result.map((subarray) => Api.post("/words", {
            data: JSON.stringify(subarray),
        }));

        //console.log(JSON.stringify(result))

        Promise.all([...promises]).then(function (values) {
            
            setMessage( values[0].data.message );
            
        });


    };

    return (
        <AuthenticatedLayout
            auth={props.auth}
            errors={props.errors}
            header={
                <h2 className="font-semibold text-xl text-gray-800 leading-tight">
                    Insert wordlist
                </h2>
            }
        >
            <Head title="Wordlist" />

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="px-8 pt-6 pb-8 mb-4 max-w-xl">
                            <form className="w-full" onSubmit={handleSubmit}>
                                <InputLabel
                                    className="pb-2"
                                    for="textarea"
                                    value="Upload *.txt file or insert words manually"
                                />

                                <input
                                    type="file"
                                    name="input"
                                    accept=".txt"
                                    id="file-input"
                                    className="pb-2"
                                    onChange={onFileChange}
                                />

                                <textarea
                                    id="textarea"
                                    rows="20"
                                    className="border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 rounded-md shadow-sm mt-1 block w-full"
                                ></textarea>

                                <div className="pt-3">
                                    <button
                                        type="submit"
                                        className="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 transition ease-in-out duration-150 false "
                                    >
                                        Submit
                                    </button>

                                    <Transition
                                        show={message != ""}
                                        enterFrom="opacity-0"
                                        leaveTo="opacity-0"
                                        className="transition ease-in-out"
                                    >
                                        <p className="text-sm pt-2 text-gray-600">
                                            {message}
                                        </p>
                                    </Transition>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
