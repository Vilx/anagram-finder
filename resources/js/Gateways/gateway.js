import axios from 'axios'

const API_URL = window.location.origin + '/api';

export default axios.create({
  baseURL: API_URL,
  timeout: 5 * 60 * 1000,
  headers: {
    'Content-Type': 'application/json; charset=utf-8'
  }
})