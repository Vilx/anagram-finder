<?php

use App\Http\Controllers\API\WordController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('words/{word}', 'App\Http\Controllers\API\WordController@show')->name('anagrams');

Route::post('words', 'App\Http\Controllers\API\WordController@store')->name('words');